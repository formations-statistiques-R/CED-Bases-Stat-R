#!bin/bash

# Construction des fichiers markdown à partir des notebooks
cd notebooks

## Introduction
jupyter nbconvert --to markdown IntroR.ipynb
cp IntroR.md ../docs/

## Manipulation de données
#nbconvert ... ManipDon.ipynb
#cp ManipDon.rmd ../docs

## Objets R
#nbconvert ... Objets.ipynb
# cp Objets.rmd ../docs

## Graphiques
# nbconvert ... GraphiqueR.ipynb
# cp GraphiqueR.rmd ../docs

# Elements de programmation
# nbconvert  ... ProgrammerR.ipynb 
# cp ProgrammerR.rmd ../docs


