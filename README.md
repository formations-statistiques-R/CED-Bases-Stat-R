## Objectifs

Ce module est destiné aux scientifiques souhaitant s'approprier les  bases de la statistique descriptive et inférentielle. 
Il se veut une initiation à la résolution de problème par une approche stochastique. Les concepts d'analyse des données, 
d'estimation et tests, d'analyse de la variance et de régression seront abordés d'un point de vue pratique. 
L'application de ces méthodes se fera à l'aide du logiciel gratuit R (http://cran.r-project.org/) et 
de son environnement RStudio (http://www.rstudio.com/).

Des notebooks seront fournis aux participants pour les cours.

## Récupérer les notebooks

    * Sur votre portable ou sur la station de la salle de formation
    
 Dans un terminal, exécutez la commande:
       git clone https://gricad-gitlab.univ-grenoble-alpes.fr/formations-statistiques-R/CED-Bases-Stat-R.git
       
    * Sur le serveur de notebooks
        ** Se connecter sur [le serveur de notebooks de GRICAD](https://jupyterhub.u-ga.fr/hub/login) avec votre compte agalan

# Partage des documents

## Comment automatiser la création des documents

 - créer/éditer les notebooks dans un répertoire sur votre portable ou sur le serveur de notebooks de GRICAD
 - commit/push 
 - verif sur https://formations-statistiques-R.gricad-pages.univ-grenoble-alpes.fr/CED-Bases-Stat-R